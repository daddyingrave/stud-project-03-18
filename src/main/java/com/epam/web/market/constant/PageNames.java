package com.epam.web.market.constant;

public class PageNames {
    public static final String USER_LIST_SHOW_PAGE = "users-list";
    public static final String USER_PROFILE_FOR_ADMIN_PAGE = "user-profile-for-admin";
    public static final String GOOD_CARD_PAGE = "good-card";
    public static final String GOOD_CARD_EDIT_PAGE = "good-card-edit";
    public static final String USER_PROFILE_FOR_ADMIN_EDIT_PAGE = "user-edit-for-admin";
    public static final String GOODS_PAGE_ADDRESS_NAME = "goods-page";
    public static final String LOGIN_PAGE_ADDRESS_NAME = "login-page";
    public static final String LOGOUT_PAGE_ADDRESS_NAME = "logout-page";
    public static final String ORDER_PAGE_ADDRESS_NAME = "orders-page";
    public static final String FORGET_PASSWORD_PAGE_NAME = "forget-password-page";
    public static final String SUCCESS_EMAIL_PASS_PAGE_NAME = "success-send-email-page";
    public static final String USER_PROFILE_SHOW_PAGE = "user-profile";
    public static final String USER_PROFILE_EDIT_PAGE = "user-edit";
    public static final String REGISTRATION_PAGE_ADDRESS_NAME = "registration-page";
    public static final String NEW_GOOD_PAGE = "new-good";
    public static final String USER_CART_SHOW_PAGE = "user-cart";
    public static final String USER_CART_EMPTY_PAGE = "user-cart-empty";
    public static final String ERROR_PAGE = "something-wrong-page";
}

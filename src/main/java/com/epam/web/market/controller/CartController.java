package com.epam.web.market.controller;

import com.epam.web.market.service.CartService;
import com.epam.web.market.service.GoodService;
import com.epam.web.market.service.UserService;
import com.epam.web.market.thymeleafform.FormCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import static com.epam.web.market.constant.PageNames.USER_CART_EMPTY_PAGE;
import static com.epam.web.market.constant.PageNames.USER_CART_SHOW_PAGE;

@Controller
public class CartController {
    private CartService cartService;
    private UserService userService;
    private GoodService goodService;

    @Autowired
    public CartController(CartService cartService, UserService userService, GoodService goodService) {
        this.cartService = cartService;
        this.userService = userService;
        this.goodService = goodService;
    }

    @Scope("session")
    @GetMapping(value = "/cart")
    public String viewCart (Model model, HttpSession session) {
            ConcurrentHashMap<Integer, Integer> userCart = cartService.getUserCart(userService.getByEmail
                    (SecurityContextHolder.getContext().getAuthentication()
                            .getName()).getId());
            List<Integer> userCartGoods = cartService.getUserCartGoods(userService.getByEmail
                    (SecurityContextHolder.getContext().getAuthentication()
                            .getName()).getId());
            session.setAttribute("goods", goodService);
            session.setAttribute("userCart", userCart);
            session.setAttribute("userCartGoods", userCartGoods);
            model.addAttribute("address", new FormCommand());
            model.addAttribute("amount", new FormCommand());

            if (cartService.getAmount(userService.getByEmail(SecurityContextHolder.getContext().getAuthentication().getName()).getId()).equals(0)) {
                return USER_CART_EMPTY_PAGE;
            }
            return USER_CART_SHOW_PAGE;
    }

    @Scope("session")
    @GetMapping(value = "/cart/remove/{id}")
    public String remove (HttpSession session, @PathVariable("id") Integer goodId) {
        cartService.remove(userService.getByEmail(SecurityContextHolder.getContext().getAuthentication().getName()).getId(), goodId);
        ConcurrentHashMap<Integer, Integer> userCart =  cartService.getUserCart(userService.getByEmail
                (SecurityContextHolder.getContext().getAuthentication()
                        .getName()).getId());
        List<Integer> userCartGoods = cartService.getUserCartGoods(userService.getByEmail
                (SecurityContextHolder.getContext().getAuthentication()
                        .getName()).getId());

        session.setAttribute("goods", goodService);
        session.setAttribute("userCart", userCart);
        session.setAttribute("userCartGoods", userCartGoods);

        if (cartService.getAmount(userService.getByEmail(SecurityContextHolder.getContext().getAuthentication().getName()).getId()).equals(0)) {
            return "redirect:/";
        }

        return "redirect:/cart";
    }

    @Scope("session")
    @PostMapping(value = "/buy")
    public String buy (HttpSession session, Model model, @ModelAttribute FormCommand form) {
        List<Integer> userCartGoods = (List<Integer>) session.getAttribute("userCartGoods");
        ConcurrentHashMap<Integer, Integer> userCart = (ConcurrentHashMap<Integer, Integer>) session.getAttribute("userCart");
        String address = form.getTextField();
        Integer userid = (userService.getByEmail(SecurityContextHolder.getContext().getAuthentication().getName()).getId());
        for (Integer userCartGood : userCartGoods) {
            cartService.setAmount(userid, userCartGood, userCart.get(userCartGood));
        }
        if (address != null && !address.equals("")) {
            System.out.println(address);
            cartService.buy(userid, address);
        } else {
            return "redirect:/cart?error=true";
        }

        if (userCartGoods.size() == 0) {
            return USER_CART_EMPTY_PAGE;
        }
        model.addAttribute("goods", goodService.getAllGoods());
        return "redirect:/orders";
    }

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @RequestMapping(value = "/good/{id}/add")
    public String editGoodCard(Model model, @PathVariable("id") Integer goodId) {
        cartService.add(userService.getByEmail(SecurityContextHolder.getContext().getAuthentication().getName()).getId(), goodId, 1);
        return "redirect:/?added";
    }

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PostMapping(value = "/cart/change/{id}")
    public String changeNumberInOrder(Model model, @PathVariable("id") Integer goodId, @ModelAttribute FormCommand amount) {
        cartService.setAmount(userService.getByEmail(SecurityContextHolder.getContext().getAuthentication().getName()).getId(), goodId, amount.getIntField());
        return "redirect:/cart";
    }
}

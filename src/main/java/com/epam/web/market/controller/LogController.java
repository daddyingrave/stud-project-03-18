package com.epam.web.market.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import static com.epam.web.market.constant.PageNames.LOGIN_PAGE_ADDRESS_NAME;
import static com.epam.web.market.constant.PageNames.LOGOUT_PAGE_ADDRESS_NAME;

@PreAuthorize("permitAll()")
@Controller
public class LogController {
    @GetMapping(value = "/login")
    public String showLogInPage() {
        return LOGIN_PAGE_ADDRESS_NAME;
    }

    @GetMapping(value = "/logout.done")
    public String showLogOutPage() {
        return LOGOUT_PAGE_ADDRESS_NAME;
    }
}

package com.epam.web.market.controller;

import com.epam.web.market.service.CartService;
import com.epam.web.market.service.OrderService;
import com.epam.web.market.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import static com.epam.web.market.constant.PageNames.ORDER_PAGE_ADDRESS_NAME;

@PreAuthorize("hasRole('USER')")
@Controller
public class OrderController {
    private OrderService orderService;
    private UserService userService;

    @Autowired
    public OrderController(OrderService orderService, UserService userService) {
        this.orderService = orderService;
        this.userService = userService;
    }

    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    @GetMapping(value = "/orders")
    public String showAllOrders(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("orderedGoods", orderService.getOrderedGoodsForUser(userService.getByEmail(auth.getName())));
        return ORDER_PAGE_ADDRESS_NAME;
    }
}

package com.epam.web.market.controller;

import com.epam.web.market.model.User;
import com.epam.web.market.service.CartService;
import com.epam.web.market.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import static com.epam.web.market.constant.PageNames.*;
import static org.thymeleaf.util.StringUtils.isEmpty;

@Controller
public class UserController {
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PreAuthorize("permitAll()")
    @GetMapping(value = "/registration")
    public String showRegistrationPage(Model model) {
        model.addAttribute("newUser", new User());
        return REGISTRATION_PAGE_ADDRESS_NAME;
    }

    @PreAuthorize("permitAll()")
    @PostMapping(value = "/registration/submit")
    public String addNewUser(@ModelAttribute User user) {
        if (!isEmpty(user.getEmail()) && !isEmpty(user.getName()) && !isEmpty(user.getPassword())) {
            if (userService.checkUserExistByEmail(user.getEmail())) {
                return "redirect:/registration?error=true";
            }
            userService.createUser(user.getName(), user.getEmail(), user.getPassword(), 1);
            return LOGIN_PAGE_ADDRESS_NAME;
        } else {
            return "redirect:/registration?error=true";
        }
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @GetMapping(value = "/profile")
    public String showProfile(Model model) {
        model.addAttribute("user", userService.getByEmail(SecurityContextHolder.getContext().getAuthentication().getName()));
        return USER_PROFILE_SHOW_PAGE;
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @GetMapping(value = "/profile/edit")
    public String editUserProfile (Model model) {
        model.addAttribute("user", userService.getByEmail(SecurityContextHolder.getContext().getAuthentication().getName()));
        model.addAttribute("updatedUser", userService.getByEmail(SecurityContextHolder.getContext().getAuthentication().getName()));
        return USER_PROFILE_EDIT_PAGE;
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @PostMapping(value = "/profile/edit/submit")
    public String submitEditedUserProfile (@ModelAttribute User user, Model model) {
        User updatedUser = userService.getByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        updatedUser = userService.changeName(updatedUser, user.getName());
        updatedUser = userService.changePassword(updatedUser, user.getPassword());
        if (!user.getEmail().equals(updatedUser.getEmail())) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            auth.setAuthenticated(false);
            SecurityContextHolder.getContext().setAuthentication(auth);
            userService.changeMail(updatedUser, user.getEmail());
            return "redirect:/logout";
        }
        model.addAttribute("user", updatedUser);
        return "redirect:/profile";
    }
}

package com.epam.web.market.daoimpl;

import com.epam.web.market.dao.GoodDAO;
import com.epam.web.market.model.Good;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GoodDAOImpl implements GoodDAO {
    private final JdbcTemplate jdbcTemplate;
    private final static RowMapper<Good> rowMapper = new BeanPropertyRowMapper<>(Good.class);

    @Autowired
    public GoodDAOImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Good> getAllGoods() {
        String sql = "SELECT `id`, `name`, `price`, `description`, `number`, `image` FROM `good`";
        return this.jdbcTemplate.query(sql, rowMapper);
    }

    @Override
    public List<Good> getGoodsByPriceRange(int x1, int x2) {
        String sql = "SELECT `id`, `name`, `price`, `description`, `number` FROM `good`, `image`  " +
                "WHERE `price`>=? AND `price`<=?";
        return this.jdbcTemplate.query(sql, rowMapper, x1, x2);
    }

    @Override
    public Good getGoodById(int id) {
        String sql = "SELECT `id`, `name`, `price`, `description`, `number`, `image` FROM `good` WHERE id=?";
        return this.jdbcTemplate.queryForObject(sql, rowMapper, id);
    }

    @Override
    public List<Good> getGoodsByNumber(int number) {
        String sql = "SELECT `id`, `name`, `price`, `description`, `number`, `image` FROM `good` WHERE number=?";
        return this.jdbcTemplate.query(sql, rowMapper, number);
    }

    @Override
    public void addGood(Good good) {
        String sql = "INSERT INTO `good` (`name`,`price`, `description`, `number`, `image`) VALUES (?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, good.getName(), good.getPrice(), good.getDescription(), good.getNumber(), good.getImage());
    }

    @Override
    public void updateGood(Good good) {
        String sql = "UPDATE `good` SET `name`=?, `price`=?, `description`=?, `number`=?, `image`=? WHERE id=?";
        jdbcTemplate.update(sql, good.getName(), good.getPrice(), good.getDescription(), good.getNumber(), good.getImage(), good.getId());
    }

    @Override
    public void updatePriceById(int id, double price) {
        String sql = "UPDATE `good` SET `price`=? WHERE id=?";
        jdbcTemplate.update(sql, price, id);
    }

    @Override
    public void updateNumberById(int id, int number) {
        String sql = "UPDATE `good` SET `number`=? WHERE id=?";
        jdbcTemplate.update(sql, number, id);
    }

    @Override
    public void deleteGood(int id) {
        String sql = "DELETE FROM `good` WHERE `id`=?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public boolean goodExists(String name, String desc) {
        String sql = "SELECT count(*) FROM `good` WHERE `name`=? AND `description`=?";
        return jdbcTemplate.queryForObject(sql, Integer.class, name, desc) != 0;
    }

    @Override
    public void updateImage(int id, byte[] image) {
        jdbcTemplate.update("UPDATE `good` SET `image`=? WHERE id=?", image, id);        
    }
}

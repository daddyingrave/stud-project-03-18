package com.epam.web.market.daoimpl;

import com.epam.web.market.dao.OrderDAO;
import com.epam.web.market.entity.Order;
import com.epam.web.market.entity.OrderedGood;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class OrderDAOImpl implements OrderDAO {
    private final JdbcTemplate template;

    @Autowired
    public OrderDAOImpl(JdbcTemplate template) {
        this.template = template;
    }

    @Override
    public void add(@NonNull Order order) {
        String query = "INSERT INTO `order` (`user_id`, `address`) VALUES (?, ?)";
        template.update(query, order.getUserId(), order.getAddress());
    }

    @Override
    public void addOrderedGood(OrderedGood orderedGood) {
        String query = "INSERT INTO `ordered_good` (`order_id`,`good_id`, `number`, `price`) VALUES (?, ?, ?, ?)";
        template.update(query, orderedGood.getOrderId(), orderedGood.getGoodId(), orderedGood.getNumber(), orderedGood.getPrice());
    }

    @Override
    public List<Order> getAll() {
        String query = "SELECT * FROM `order`";

        List<Order> orders = new ArrayList<Order>();

        List<Map<String, Object>> rows = template.queryForList(query);
        for (Map row : rows) {
            orders.add(new Order((Integer) row.get("id"), (Integer) row.get("user_id"), (String) row.get("address")));
        }
        return orders;
    }

    @Override
    public Order getById(@NonNull Integer id) {
        String query = "SELECT * FROM `order` WHERE `id` = ?";
        return template.queryForObject(query, new Object[]{id}, new BeanPropertyRowMapper<>(Order.class));
    }

    @Override
    public List<OrderedGood> getOrderedGoods() {
        String query = "SELECT * FROM `ordered_good`";

        List<OrderedGood> orders = new ArrayList<OrderedGood>();

        List<Map<String, Object>> rows = template.queryForList(query);
        for (Map<String, Object> row : rows) {
            orders.add(new OrderedGood((Integer) row.get("order_id"), (Integer) row.get("good_id"),
                    (Integer) row.get("number"), (BigDecimal) row.get("price")));
        }

        return orders;
    }

    @Override
    public boolean checkOrderExist(Order order) {
        String query = "SELECT COUNT(*) FROM `order` WHERE `id` = ?";
        Integer number = template.queryForObject(query, Integer.class, order.getId());
        return number != 0;
    }

    @Override
    public void update(@NonNull Order order) {
        String query = "UPDATE `order` SET `id` = ?, `user_id` = ?, `address` = ? WHERE `id` = ?";
        template.update(query, order.getId(), order.getUserId(), order.getAddress(), order.getId());
    }

    @Override
    public void deleteByOrder(@NonNull Order order) {
        String query = "DELETE FROM `order` WHERE `id` = ? AND `user_id` = ? AND `address` = ?";
        template.update(query, order.getId(), order.getUserId(), order.getAddress());
    }

    @Override
    public void deleteById(@NonNull Integer id) {
        String query = "DELETE FROM `order` WHERE `id` = ?";
        template.update(query, id);
    }
}

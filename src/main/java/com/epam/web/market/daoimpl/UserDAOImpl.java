package com.epam.web.market.daoimpl;

import com.epam.web.market.dao.UserDAO;
import com.epam.web.market.model.User;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.epam.web.market.encoder.Encoder.decode;
import static com.epam.web.market.encoder.Encoder.encode;


@Repository
public class UserDAOImpl implements UserDAO {

    private JdbcTemplate jdbc;
    private BeanPropertyRowMapper<User> rowmapper = BeanPropertyRowMapper.newInstance(User.class);

    @Autowired
    public UserDAOImpl(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public User getById(@NonNull Integer id) {
        String statement = "SELECT `id`, `name`, `password`, `blacklisted`, `group_id`, `email` " +
                "FROM `user` " +
                "WHERE `id` = ?";
        User user = jdbc.queryForObject(statement, new Object[]{id}, rowmapper);
        user.setPassword(decode(user.getPassword()));
        return user;
    }

    @Override
    public User getByEmail(@NonNull String email) {
        String statement = "SELECT `id`, `name`, `password`, `blacklisted`, `group_id`, `email` " +
                "FROM `user` " +
                "WHERE `email` = ?";
        User user = jdbc.queryForObject(statement, new Object[]{email}, rowmapper);
        user.setPassword(decode(user.getPassword()));
        return user;
    }

    @Override
    public void insert(@NonNull User user) {
        String statement = "INSERT INTO `user` (`name`, `password`, `blacklisted`, `group_id`, `email`) " +
                "VALUES (?, ?, ?, ?, ?)";
        jdbc.update(statement, user.getName(), encode(user.getPassword()), user.getBlacklisted(), user.getGroupId(), user.getEmail());
    }

    @Override
    public List<User> getAll() {
        String statement = "SELECT `id`, `name`, `password`, `blacklisted`, `group_id`, `email` " +
                "FROM `user`";
        List<Map<String, Object>> rows = jdbc.queryForList(statement);
        return userRowMap(rows);
    }

    @Override
    public List<User> getByGroupId(@NonNull Integer group_id) {
        String statement = "SELECT `id`, `name`, `password`, `blacklisted`, `group_id`, `email` " +
                "FROM `user` " +
                "WHERE `group_id` = " + group_id;
        List<Map<String, Object>> rows = jdbc.queryForList(statement);
        return userRowMap(rows);
    }

    private ArrayList<User> userRowMap(@NonNull List<Map<String, Object>> rows) {
        ArrayList<User> users = new ArrayList<>();
        for (Map row : rows) {
            User user = new User((int) row.get("id"));
            user.setName((String) row.get("name"));
            user.setPassword(decode((String) row.get("password")));
            user.setBlacklisted((boolean) row.get("blacklisted"));
            user.setGroupId((int) row.get("group_id"));
            user.setEmail((String) row.get("email"));
            users.add(user);
        }
        return users;
    }

    @Override
    public void update(@NonNull User user) {
        user.setPassword(encode(user.getPassword()));

        StringBuilder sqlbuilder = new StringBuilder(1000);
        sqlbuilder.append("UPDATE `user` SET");

        if (user.getName() != null) {
            sqlbuilder.append(" `name` = '" + user.getName() + "',");
        }

        if (user.getEmail() != null) {
            sqlbuilder.append(" `email` = '" + user.getEmail() + "',");
        }
        if (user.getPassword() != null) {
            sqlbuilder.append(" `password` = '" + user.getPassword() + "',");
        }

        if (user.getBlacklisted() != null) {
            sqlbuilder.append(" `blacklisted` = " + user.getBlacklisted() + ",");
        }
        if (user.getGroupId() != null) {
            sqlbuilder.append(" `group_id` = " + user.getGroupId() + "");
        }
        if (sqlbuilder.charAt(sqlbuilder.length() - 1) == ',') {
            sqlbuilder.deleteCharAt(sqlbuilder.length() - 1);
        }

        sqlbuilder.append(" WHERE id = " + user.getId());
        jdbc.update(sqlbuilder.toString());
    }

    @Override
    public boolean checkUserExistByEmail(String email) {
        String query = "SELECT COUNT(*) FROM `user` WHERE `email` = ?";
        Integer number = jdbc.queryForObject(query, Integer.class, email);
        return number != 0;
    }


    @Override
    public void deleteById(@NonNull Integer id) {
        String statement = "DELETE FROM `user` " +
                "WHERE `id` = " + id;
        jdbc.update(statement);
    }

    @Override
    public void deleteAll() {
        String statement = "DELETE FROM `user`";
        jdbc.update(statement);
    }

}


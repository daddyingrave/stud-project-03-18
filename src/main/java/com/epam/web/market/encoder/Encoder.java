package com.epam.web.market.encoder;

public class Encoder {
    public static String encode(String password){
        StringBuilder builder = new StringBuilder(password);
        StringBuilder encodeBuilder = new StringBuilder("");
        for (int i = 0; i < builder.length(); ++i){
            encodeBuilder.append((char) (builder.charAt(i)*2 + 1));
        }
        return encodeBuilder.toString();
    }

    public static String decode(String password){
        StringBuilder builder = new StringBuilder(password);
        StringBuilder decodeBuilder = new StringBuilder("");
        for (int i = 0; i < builder.length(); ++i){
            decodeBuilder.append((char) ((builder.charAt(i) - 1)/2));
        }
        return decodeBuilder.toString();
    }
}

START TRANSACTION;
USE `mydb`;
INSERT INTO `good` (`name`,`price`, `description`, `number`) VALUES ('Cardigan for men The North Face Surgent', 4999, 'Convenient technological hudy from The North Face - an excellent choice for outdoor activities and hikes.', 1000);
INSERT INTO `good` (`name`,`price`, `description`, `number`) VALUES ('Cardigan for men Termit', 349, 'Male snowboard jumper from Termit.', 1000);
INSERT INTO `good` (`name`,`price`, `description`, `number`) VALUES ('Cardigan for men Glissade', 649, 'Men\'s cardigan for skiing from Glissade.', 1000);
INSERT INTO `good` (`name`,`price`, `description`, `number`) VALUES ('Cardigan for men Nike Dry', 1999, 'Male jumper for training from Nike.', 1000);
INSERT INTO `good` (`name`,`price`, `description`, `number`) VALUES ('Cardigan for men Termit', 499, 'Comfortable men\'s snowboard jumper.', 1000);
INSERT INTO `good` (`name`,`price`, `description`, `number`) VALUES ('Cardigan for men Demix', 1849, 'A warm sports jumper from Demix is an excellent choice for cool weather.', 1000);
INSERT INTO `good` (`name`,`price`, `description`, `number`) VALUES ('Sweatshirt for women Glissade', 449, 'A practical jumper from Glissade is an excellent choice for skiing.', 1000);
INSERT INTO `good` (`name`,`price`, `description`, `number`) VALUES ('Cardigan for women Kappa', 1499, 'A sweatshirt with a zipper from Kappa will perfectly fit into your sports wardrobe.', 1000);
INSERT INTO `good` (`name`,`price`, `description`, `number`) VALUES ('Cardigan for women Termit', 349, 'Female snowboard jumper from Termit.', 1000);
INSERT INTO `good` (`name`,`price`, `description`, `number`) VALUES ('Sweatshirt for women Glissade', 649, 'Female fleece sweater Glissade is suitable for skiing.', 1000);
COMMIT;

START TRANSACTION;
USE `mydb`;
INSERT INTO `group` (`name`) VALUES ('Client');
INSERT INTO `group` (`name`) VALUES ('Admin');
COMMIT;

START TRANSACTION;
USE `mydb`;
INSERT INTO `user` (`name`, `password`, `blacklisted`, `group_id`, `email`) VALUES ('Elvis Presley', 'klj34kjp', FALSE, 1, 'Elvis@gmail.com');
INSERT INTO `user` (`name`, `password`, `blacklisted`, `group_id`, `email`) VALUES ('Muddy Waters', 'asdo2pt5', FALSE, 1, 'Muddy@gmail.com');
INSERT INTO `user` (`name`, `password`, `blacklisted`, `group_id`, `email`) VALUES ('Janis Joplin', 'dfjvk7df9', FALSE, 1, 'Janis@gmail.com');
INSERT INTO `user` (`name`, `password`, `blacklisted`, `group_id`, `email`) VALUES ('Jack Leroy Wilson', 'dkjehv65fh', FALSE, 2, 'Jack@gmail.com');
COMMIT;

START TRANSACTION;
USE `mydb`;
INSERT INTO `status_type` (`name`) VALUES ('new');
INSERT INTO `status_type` (`name`) VALUES ('on the way');
INSERT INTO `status_type` (`name`) VALUES ('waiting for payment');
INSERT INTO `status_type` (`name`) VALUES ('ready');
INSERT INTO `status_type` (`name`) VALUES ('finished');
INSERT INTO `status_type` (`name`) VALUES ('canceled');
COMMIT;